{-# LANGUAGE ForeignFunctionInterface #-}

module Main where

import ParserBase
import ParserSystem

import Text.Parsec
import Text.Parsec.String
import Text.ParserCombinators.Parsec.Prim hiding (try)
import Text.ParserCombinators.Parsec.Number
import Text.ParserCombinators.Parsec.Error
import qualified TargetMath as Math
import qualified TargetScipy as Scipy

import Foreign.C.String

-- doesn't really do anything, but has lots of commented out parser tests
main :: IO ()
main = do
  -- test "math" "1+ 2"
  -- parseTest parseFactor "5"
  -- parseTest parseFactor "omega1"
  -- parseTest parseTerm "5*4"
  -- parseTest parseExpr "1+2"
  -- parseTest parseExpr "1+2*4"
  -- parseTest parseExpr "-2.4"
  -- parseTest parseExpr "1 + 2"
  -- parseTest parseExpr "1+ 2"
  -- parseTest parseExpr "(1+2) * 8"
  -- parseTest parseExpr "(hat + 4/2)"
  -- -- parseTest parseStmt "34* 4 9" -- should error
  -- parseTest parseStmt "2 ^ 4"
  -- parseTest parseStmt "( 1+2 ) * 3^2"
  -- parseTest parseStmt "exp(4)"
  -- parseTest parseStmt "exp( theta/2 )"
  -- parseTest parseStmt "sin(2*pi)"
  return ()

-- wrapper for getting things in and out of C
compileTo :: CString -> CString -> CString -> CString -> IO CString
compileTo tgt fname expr funcname =
  (compileTo_hs (peekCString tgt) (peekCString fname) (peekCString expr) (peekCString funcname)) >>= newCString

-- the real compiler function; chooses whether to read from file or use expr
compileTo_hs :: IO String -> IO String -> IO String -> IO String -> IO String
compileTo_hs tgt fname expr funcname = do
  f <- fname
  func <- funcname
  if f /= ""
    then do
      t <- tgt
      e <- readFile f
      case (target t e func) of
        Left err -> do
          error (show err)
        Right func -> do
          --putStrLn func
          return func
    else do
      t <- tgt
      e <- expr
      case (target t e func) of
        Left err -> do
          error (show err)
        Right func -> do
          --putStrLn func
          return func

-- parses expression, chooses which set of target functions to call
target :: String -> String -> String -> Either ParseError String
target t expr funcname
  -- parsing out to math is for testing only, it can't do systems
  | t == "math" = do
      e <- parse parseStmt "" expr
      return (show (Math.evalExpr e))
  | t == "scipy" = do
      e <- parse parseSystem "" expr
      return (Scipy.evalSystem e funcname)

foreign export ccall compileTo :: CString -> CString -> CString -> CString -> IO CString
