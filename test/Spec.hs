import Test.QuickCheck
import ParserBase
import Text.ParserCombinators.Parsec.Prim
import Data.Either

main :: IO ()
main = do
  putStrLn "testing const"
  quickCheck constTest

  putStrLn "testing var"
  quickCheck varTest

constTest :: Double -> Bool
constTest x = (parse parseConst "" (show x)) == Right (Const x)

varTest :: String -> Bool
varTest x = (parse parseVar "" x) == Right (Var x)
