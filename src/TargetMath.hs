module TargetMath where

import ParserBase
import ParserSystem

-- mostly for testing
-- doesn't support variables or equations because you can't make those
-- into doubles

evalExpr :: Expr -> Double
evalExpr expr = case expr of
  Ter t -> evalTerm t
  Plus t e -> (evalTerm t) + (evalExpr e)
  Minus t e -> (evalTerm t) - (evalExpr e)

evalTerm :: Term -> Double
evalTerm term = case term of
  Func f -> evalFunction f
  Mult f t -> (evalFunction f) * (evalTerm t)
  Div f t -> (evalFunction f) / (evalTerm t)

evalFunction :: Function -> Double
evalFunction fun = case fun of
  Fact f -> evalFactor f
  Expo f1 f2 -> (evalFactor f1) ** (evalFunction f2)
  MathFunc fname e -> (callFname fname (evalExpr e))

evalFactor :: Factor -> Double
evalFactor factor = case factor of
  Const c -> c
  Paren e -> evalExpr e

callFname :: Fname -> Double -> Double
callFname f n
  | f == "exp" = exp(n)
  | f == "sin" = sin(n)
  | f == "cos" = cos(n)
  | f == "tan" = tan(n)
