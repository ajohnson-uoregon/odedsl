module ParserSystem where

import Text.Parsec hiding (try)
import Text.Parsec.String
import Text.ParserCombinators.Parsec.Prim
import Text.ParserCombinators.Parsec.Number

import ParserBase

-- Entire grammar:
-- <file>       ::= <eqn> | <system> | <eqn> <coeff> | <system> <coeff>
-- <coeff>      ::= "{" (<binding> ";")+ "}"
-- <system>     ::= "{" (<eqn> ";")+ "}" -- regex shorthand for "match <eqn>;" one or more times
-- <binding>    ::= <variable> "=" <constant>
-- <eqn>        ::= <variable> "'" "=" <expr> | <variable> "'(" <variable> ")" "=" <expr>

-- <expr>	      ::= <term> "+" <expr> | <term> "-" <expr> | <term>
-- <term>     	::= <function> "*" <term> | <function> "/" <term> | <function>
-- <function>   ::= <factor> "^" <function> | <fname> "(" <expr> ")" | <factor>
-- <factor>     ::= <constant> | <variable> | "(" <expr> ")"
-- <variable>   ::= String
-- <constant>   ::= Double

data Eqn = BasicEq Variable Expr
         | EqOf Variable Variable Expr
  deriving (Show)

type System = [Eqn]

--------------------------------------------
-- <eqn>        ::= <variable> "=" <expr> | <variable> "'(" <variable> ")" "=" <expr>
--------------------------------------------

parseBasicEq :: Parser Eqn
parseBasicEq = do
  v <- parseVariable
  char '\''
  spaces
  char '='
  spaces
  e <- parseExpr
  return (BasicEq v e)

-- this isn't exactly used, but exists for future support of this type of equation
parseEqOf :: Parser Eqn
parseEqOf = do
  v1 <- parseVariable
  char '\''
  char '('
  v2 <- parseVariable
  char ')'
  spaces
  char '='
  spaces
  e <- parseExpr
  return (EqOf v1 v2 e)

parseEqn :: Parser Eqn
parseEqn = (try parseBasicEq) <|> parseEqOf

---------------------------------------
-- <system>     ::= "{" (<eqn> ";")+ "}" -- regex shorthand for "match <eqn>;" one or more times
------------------------------------------------

-- parse many <eqn> ";" and {}
parseSystem :: Parser System
parseSystem = do
  char '{'
  spaces
  eqns <- many1 parseSysEqn
  spaces
  char '}'
  return eqns

-- parse a single <eqn> ";"
parseSysEqn :: Parser Eqn
parseSysEqn = do
  eq <- parseEqn
  char ';'
  spaces
  return eq
