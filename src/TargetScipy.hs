module TargetScipy where

import ParserBase
import ParserSystem

import qualified Data.Map as Map

-- alias so it's clear what are actually functions and what are just strings
type PythonFunc = String

evalSystem :: System -> String -> PythonFunc
evalSystem sys funcname =
  "def " ++ funcname ++ "(t, y):\n\t" ++ (vars . varList $ sys) ++ " = y\n\tdydt = [" ++ (evalEqns sys) ++ "]\n\treturn dydt\n"

-- first thing to do is get a list of all the left-hand sides so python can unpack y
-- future improvement could be to condense this to a single function ala evalEqns
vars :: [Variable] -> String
vars [] = ""
vars (v:vs) = v ++ ", " ++ (vars vs)

varList :: System -> [Variable]
varList [] = []
varList (e:es) = case e of
  BasicEq v expr -> v:(varList es)
  EqOf v1 v2 expr -> v1:(varList es)

-- turn all the various equations into a single string-list
evalEqns :: System -> String
evalEqns [] = ""
evalEqns (e:es) = (evalEqn e) ++ ", " ++ (evalEqns es)

evalEqn :: Eqn -> String
evalEqn e = case e of
  BasicEq v expr -> evalExpr expr
  EqOf v1 v2 expr -> evalExpr expr

evalExpr :: Expr -> String
evalExpr e = case e of
  Ter t -> evalTerm t
  Plus t e -> evalTerm t ++ "+" ++ evalExpr e
  Minus t e -> evalTerm t ++ "-" ++ evalExpr e

evalTerm :: Term -> String
evalTerm t = case t of
  Func f -> evalFunction f
  Mult f t -> evalFunction f ++ "*" ++ evalTerm t
  Div f t -> evalFunction f ++ "/" ++ evalTerm t

evalFunction :: Function -> String
evalFunction f = case f of
  Fact factor -> evalFactor factor
  Expo fact fun -> "(" ++ evalFactor fact ++ ")^(" ++ evalFunction fun ++ ")"
  MathFunc fname expr -> evalFname fname ++ "(" ++ evalExpr expr ++ ")"

evalFactor :: Factor -> String
evalFactor f = case f of
  Const c -> show c
  Var v -> v
  Paren expr -> "(" ++ evalExpr expr ++ ")"

-- map for translating internal function names to python (numpy) function names
fnameToPython :: Map.Map Fname String
fnameToPython = Map.fromList [("sin", "np.sin"),
                          ("cos", "np.cos"),
                          ("tan", "np.tan"),
                          ("exp", "np.exp")]

evalFname :: Fname -> String
evalFname name = case Map.lookup name fnameToPython of
  Just n -> n
  Nothing -> name -- if we don't find anything, just give the name back
