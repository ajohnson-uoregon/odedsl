module ParserBase where

import Text.Parsec hiding (try)
import Text.Parsec.String
import Text.ParserCombinators.Parsec.Prim
import Text.ParserCombinators.Parsec.Number

-- Base grammar:
-- <expr>	      ::= <term> "+" <expr> | <term> "-" <expr> | <term>
-- <term>     	::= <function> "*" <term> | <function> "/" <term> | <function>
-- <function>   ::= <factor> "^" <function> | <fname> "(" <expr> ")" | <factor>
-- <factor>     ::= <constant> | <variable> | "(" <expr> ")"
-- <variable>   ::= String
-- <constant>   ::= Double

data Expr = Ter Term
          | Plus Term Expr
          | Minus Term Expr
  deriving (Show)

data Term = Func Function
          | Mult Function Term
          | Div Function Term
  deriving (Show)

data Function = Fact Factor
              | Expo Factor Function
              | MathFunc Fname Expr
  deriving (Show)

type Fname = String

data Factor = Const Constant
            | Var Variable
            | Paren Expr
  deriving (Show)

type Variable = String

type Constant = Double

-----------------------------------------------

instance Eq Factor where
  Const x == Const y = x == y
  Var x == Var y = x == y
  Paren ex1 == Paren ex2 = ex1 == ex2
  _ == _ = False

instance Eq Function where
  Fact x == Fact y = x == y
  Expo x y == Expo z w = x == z && y == w
  MathFunc x y == MathFunc z w = x == z && y == w
  _ == _ = False

instance Eq Term where
  Func x == Func y = x == y
  Mult x y == Mult z w = x == z && y == w
  Div x y == Div z w = x == z && y == w
  _ == _ = False

instance Eq Expr where
  Ter x == Ter y = x == y
  Plus x y == Plus z w = x == z && y == w
  Minus x y == Minus z w = x == z && y == w
  _ == _ = False

----------------------------------------------
-- <variable>   ::= String
-- <constant>   ::= Double
-------------------------------------------------

parseConstant :: Parser Constant
parseConstant = do
  s <- sign
  x <- floating3 False -- false means there might not be a digit after the decimal
  return (s x)

parseVariable :: Parser Variable
parseVariable = do
  name <- many1 alphaNum
  return name

-----------------------------------------
-- <factor>     ::= <constant> | <variable> | "(" <expr> ")"
------------------------------------------

parseConst :: Parser Factor
parseConst = do
  c <- parseConstant
  return (Const c)

parseVar :: Parser Factor
parseVar = do
  name <- parseVariable
  return (Var name)

parseParen :: Parser Factor
parseParen = do
  char '('
  spaces
  x <- parseExpr
  spaces
  char ')'
  return (Paren x)

parseFactor :: Parser Factor
parseFactor = (try parseConst) <|> (try parseVar) <|> parseParen

--------------------------------------------
-- <function>   ::= <factor> "^" <function> | <fname> "(" <expr> ")" | <factor>
----------------------------------------------

parseFact :: Parser Function
parseFact = do
  f <- parseFactor
  return (Fact f)

parseExpo :: Parser Function
parseExpo = do
  f1 <- parseFactor
  spaces
  char '^'
  spaces
  f2 <- parseFunction
  return (Expo f1 f2)

parseMathFunc :: Parser Function
parseMathFunc = do
  fun <- parseFname
  char '('
  spaces
  e <- parseExpr
  spaces
  char ')'
  return (MathFunc fun e)

-- a bit of trickery to make parsers out of this list of valid function names:
valid_fnames :: [Fname]
valid_fnames = ["sin", "cos", "tan", "exp"]

-- make a list of parsers
fnameparsers :: [Parser Fname]
fnameparsers = [string s | s <- valid_fnames]

-- chain those parsers together
parseFnameHelp :: [Parser Fname] -> Parser Fname
parseFnameHelp [p] = p
parseFnameHelp (p:ps) = try p <|> parseFnameHelp ps

parseFname :: Parser Fname
parseFname = parseFnameHelp fnameparsers

parseFunction :: Parser Function
parseFunction = (try parseExpo) <|> (try parseMathFunc) <|> parseFact

---------------------------------------------
-- <term>     	::= <function> "*" <term> | <function> "/" <term> | <function>
-----------------------------------------------

parseFunc :: Parser Term
parseFunc = do
  f <- parseFunction
  return (Func f)

parseMult :: Parser Term
parseMult = do
  t <- parseFunction
  spaces
  char '*'
  spaces
  f <- parseTerm
  return (Mult t f)

parseDiv :: Parser Term
parseDiv = do
  t <- parseFunction
  spaces
  char '/'
  spaces
  f <- parseTerm
  return (Div t f)

parseTerm :: Parser Term
parseTerm = (try parseMult) <|> (try parseDiv) <|> parseFunc

------------------------------------------------
-- <expr>	      ::= <term> "+" <expr> | <term> "-" <expr> | <term>
-------------------------------------------------

parseTer :: Parser Expr
parseTer = do
  t <- parseTerm
  return (Ter t)

parsePlus :: Parser Expr
parsePlus = do
  e <- parseTerm
  spaces
  char '+'
  spaces
  t <- parseExpr
  return (Plus e t)

parseMinus :: Parser Expr
parseMinus = do
  e <- parseTerm
  spaces
  char '-'
  spaces
  t <- parseExpr
  return (Minus e t)

parseExpr :: Parser Expr
parseExpr = (try parsePlus) <|> (try parseMinus) <|> parseTer

-- make sure we get to the end of the string; mostly for testing
parseStmt = parseExpr <* eof
