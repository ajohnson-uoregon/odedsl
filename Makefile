GHC=ghc
GHC_RUNTIME_LINKER_FLAG=-lHSrts-ghc7.10.3
PACKAGE_DB=.cabal-sandbox/x86_64-linux-ghc-7.10.3-packages.conf.d
PACKAGES=-package parsec -package parsec-numbers

lib.so: app/Main.o src/ParserBase.o src/ParserSystem.o src/TargetScipy.o src/TargetMath.o wrapper.o
	$(GHC) -o $@ -shared -dynamic -fPIC $^ -package-db $(PACKAGE_DB) $(PACKAGES) $(GHC_RUNTIME_LINKER_FLAG)

app/Main_stub.h app/Main.o:
	$(GHC) --make -dynamic -fPIC -isrc -package-db $(PACKAGE_DB) app/Main.hs

wrapper.o: wrapper.c
	$(GHC) -c -dynamic -fPIC wrapper.c

clean:
	rm -f *.hi *.o *_stub.[ch] app/*.hi app/*.o app/*_stub.[ch] src/*.hi src/*.o src/*_stub.[ch]

clean-all:
	rm -f *.hi *.o *_stub.[ch] *.so


# Runs the example Python program
example: libffi-example.so
	python program.py
