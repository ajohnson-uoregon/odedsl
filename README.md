# ODEDSL

This is a domain-specific language (DSL) for ordinary differential equations (ODEs). It's implemented in 
Haskell using the Parsec library and Haskell's FFI interface for C, which means it can be called from 
C/C++ (theoretically, untested) and Python (works, has been tested). It can currently target mathematical
expressions (mostly for testing purposes, does not support equations or systems) and SciPy's solve_ivp function. 

## Usage

#### Syntax

Only (systems of) first-order DEs are supported, because many solvers only accept such systems, and transformation from a 
higher-order equation to a system of first order equations is trivial. 

In general, the syntax for a system of equations looks like this:

```
{ <var1>' = <expression1>;
  <var2>' = <expression2>;
  ... }
```

Before the equals sign, there should be a single variable (any string of letters and/or numbers) and since this is a 
differential equation, we also assume there's an apostrophe (prime) 
marking that this is the derivative of that variable. After the equals sign is any mathematical expression followed by 
a semicolon marking the end of that equation. Whitespace does not matter; there doesn't need to be space around the 
equals, between equations, or around operators in the expressions (however, whitespace is encouraged for readablity).

Allowed operators for expressions:

- \+
- \-
- \* (must have \* - there is **no implicit multiplication**)
- /
- ^
- sin(<expr\>)
- cos(<expr\>)
- tan(<expr\>)
- exp(<expr\>) (e^x)
- (<expr\>) (parentheticals)

The parser knows about operator precedence, so if you're compiling down to math, it knows that 1+2\*4 is 9, not 12. However, 
when compiling down to a different language, like Python, it assumes that language *also* knows about precedence, so 
parenthesize your systems accordingly.

#### Calling from Python

If you want to use the wrapper found in examples/odedsl.py, import it and then you can call `runCompiler` with the 
keyword arguments `target`, `file_name` or `expr`, and `function_name`. `target` sets which language to compile to, `function_name` 
sets the name of the function the compiler will output (if nothing is passed in, it will default to "fun"), 
and `file_name`/`expr` tells the compiler to read a system from 
that file or use the expression passed in. `runCompiler` will return a string containing the function, which you can 
then `exec` to get a function in your current namespace that you can call. The other examples show more about 
how this works.

If you don't want to use the wrapper (importing it can be tricky because it's not really a package, yet), keep reading.

Calling from Python depends on Python's foreign function interface (FFI) for C, which can be found in its ctypes library.
To load the compiled Haskell library, call `cdll.LoadLibrary("<path to\>/lib.so")`. Afterwards, you have to start up the 
Haskell environment by calling `haskell_init()` from the loaded library and then tell Python that the `compileTo` function
(which does the work of compiling the DSL) returns a string by running `lib.compileTo.restype = c_char_p`. 
Python's C FFI assumes all functions return ints, which is not true for this, and if you don't do this you'll get 
very confusing results. After you're done with the library, call `haskell_exit()` to shut down the Haskell 
environment. 

The `compileTo` function takes four arguments. The first is a string telling it what to compile the DSL to; currently, the 
two accepted values are "math" and "scipy". The second argument is for passing in a filepath for the compiler to read from, 
the third argument is for passing in a system directly. If a filepath is specified, the third argument will be ignored. The last argument
is a name for the function it'll return. If you don't pass in anything, you'll get a bad function out.

compileTo will return a string containing the compiled system, or will crash with a parse error from Haskell. 
To turn the string in to a real function 
in your current namespace, call `exec()` on it. This will evaluate the string as Python code, adding a function 
with the specified name that you can call and pass to `scipy.integrate.solve_ivp`.

For a full example that graphs the result using matplotlib, see the examples directory. There are examples for a 
pendulum with damping, exponential decay, and the Lorenz equations. These use the wrapper, but if you don't want to you 
can borrow the code from the wrapper to call `compileTo` directly.

## Installation 

Full directions can be found in INSTALL.txt at the top level of the repo.

#### Dependencies

Haskell:

- ghc
- cabal
- parsec
- parsec-numbers

Python:

- numpy (if sin, cos, exp, etc. functions are used, assumes numpy is installed and imported as np)
- ctypes (Python's FFI for C)

### Future Additions

- Support for 'functions of a variable,' i.e., functions written as x'(t) = -0.5\*x(t). Currently only supports x' = -0.5\*x.
- Automatic vectorization. SciPy supports solving vectorized systems (simultaneously solving multiple instances of the same 
	system), but requires some changes to the function it takes in. These changes are well-defined and could be made automatic.
- Separate coefficients. For large systems with many coefficients (some of which are the same), it would be good to have 
	coefficients defined separately from the system for ease of modifying them. So people like me don't forget to change 
	a coefficient in one place and then get bad resutls.
- Target other languages, such as:
	- SciPy's odeint
	- Matlab?
	- Boost's odeint?
- Make installation nicer. You shouldn't have to modify a makefile.
- Better Python wrapper. Right now it's a little hacky to import because it isn't a real paackage.
- A `use_numpy` flag, so users can choose whether or not they want to get numpy functions in their compiled function or not.

### Acknowledgements

The example makefile and wrapper.c from https://github.com/nh2/haskell-from-python were extremely helpful in figuring out how to call 
Haskell from Python. 