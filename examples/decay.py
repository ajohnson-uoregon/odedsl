import numpy as np
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt

import odedsl

fun_string = odedsl.runCompiler(target="scipy", file_name="decay.txt")
print(fun_string)
exec(fun_string)

y0 = [100.0]
t = np.arange(0, 10, 0.01)

sol = solve_ivp(fun, [0,10], y0, t_eval=t)
#sol = scipy.integrate.odeint(fun1, y0, t)
#print(sol)

plt.plot(t, sol.y[0], 'b', label='')
#plt.plot(t, sol.y[1], 'g', label='omega(t)')
plt.legend(loc='best')
plt.xlabel('t')
plt.grid()
plt.show()
