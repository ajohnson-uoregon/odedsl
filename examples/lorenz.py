import numpy as np
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import odedsl

fun_string = odedsl.runCompiler(target="scipy", file_name="lorenz.txt", function_name="lorenz")
print(fun_string)
exec(fun_string)

y0 = [1.0, 1.0, .98]
t = np.arange(0, 40, 0.01)

sol = solve_ivp(lorenz, [0,40], y0, t_eval=t)
#sol = scipy.integrate.odeint(fun1, y0, t)
#print(sol)

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot(sol.y[0], sol.y[1], sol.y[2])
plt.show()
