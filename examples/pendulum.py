import numpy as np
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt

import odedsl

func = odedsl.runCompiler(target="scipy", file_name="pendulum.txt", function_name="pendulum")
print(func)
exec(func)

y0 = [np.pi-0.1, 0.0]
t = np.linspace(0, 10, 101)

sol = solve_ivp(pendulum, [0,10], y0, t_eval=t)
#sol = scipy.integrate.odeint(fun1, y0, t)
#print(sol)

plt.plot(t, sol.y[0], 'b', label='theta(t)')
plt.plot(t, sol.y[1], 'g', label='omega(t)')
plt.legend(loc='best')
plt.xlabel('t')
plt.grid()
plt.show()
