from ctypes import *

def runCompiler(target="scipy", file_name="", expr="", function_name="fun"):

    if file_name == "" and expr == "":
        raise ValueError("file_name and expr cannot both be empty - must pass in something to compile")

    lib = cdll.LoadLibrary("../lib.so")

    lib.haskell_init()
    lib.compileTo.restype = c_char_p

    fun_string = lib.compileTo(target, file_name, expr, function_name)

    lib.haskell_exit()

    return fun_string
