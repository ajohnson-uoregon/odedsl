{
  theta' = omega;
  omega' = -.25*omega - 5*sin(theta);
}
